package test.amazon.brand;

import java.io.IOException;
import java.util.Optional;

public interface AmazonBrandNameReader {
    Optional<AmazonBrandNameParser.BrandInfo> getBrandInfo(String siteUrl) throws IOException;
}
