package test.amazon.brand;

import lombok.Data;
import lombok.experimental.Accessors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import test.TestRunner;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AmazonBrandNameParser implements AmazonBrandNameReader {

    /*
        https://www.amazon.com/stores/node/2530006011?_encoding=UTF8&field-lbr_brands_browse-bin=Nike&ref_=bl_sl_s_sh_web_2530006011
    */
    static List<String> regex = List.of(
            //Visit the AmazonBasics Store
            "Visit\\s+the\\s+(.*)\\s+Store",
            //Brand: STANLEY
            "Brand:\\s+(.*)?\\s*"
    );

    static List<Pattern> patterns = regex.stream()
            .map(Pattern::compile)
            .collect(Collectors.toUnmodifiableList());


    @Data
    @Accessors(chain = true)
    static class BrandInfo {
        String brandUrl;
        String brandName;
    }

    @Override
    public Optional<BrandInfo> getBrandInfo(String siteUrl) throws IOException {
        TestRunner.StopWatch stopWatch = new TestRunner.StopWatch();
        stopWatch.start("download ");
        Document doc = Jsoup.connect(siteUrl)
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36")
                .header("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
                .get();

        var textWithBrandNameElement = Optional.of(doc.select("#bylineInfo"))
                .filter(elements -> elements.size() > 0)
                .map(elements -> elements.get(0))
                .orElse(null);
        if (textWithBrandNameElement == null) {
            System.err.println("No Element #bylineInfo");
            return Optional.empty();
        }

        String brandNameText = textWithBrandNameElement.text();

        String brandLink = textWithBrandNameElement.attr("href");
        String brandName = patterns.stream()
                .map(pattern -> pattern.matcher(brandNameText))
                .filter(Matcher::find)
                .map(m -> m.group(1))
                .findAny()
                .orElse(null);
        BrandInfo brandInfo = new BrandInfo()
                .setBrandUrl(brandLink)
                .setBrandName(brandName);
        System.out.println(siteUrl + " :\n\t" + brandInfo);
        return Optional.of(brandInfo);
    }
}
