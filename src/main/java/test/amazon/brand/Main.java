package test.amazon.brand;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import test.TestRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    static List<String> urls = List.of(
            //----          Brand: STANLEY
//          PRODUCTS
            "https://www.amazon.com/NIKE-Mens-Vapormax-Black-Black-Dark/dp/B005ANMORO/ref=sr_1_3?crid=3MP4RIQARJDCH&dchild=1&keywords=nike+vapormax&qid=1595947063&sprefix=nike+va%2Caps%2C248&sr=8-3",
            //    Nike
            "https://www.amazon.com/Amway-GLISTER-MULTI-ACTION-FLUORIDE-TOOTHPASTE/dp/B007NJ2MFC/ref=sr_1_3?crid=5F5M61317JG5&dchild=1&keywords=amway+toothpaste&qid=1595947156&sprefix=amway%2Caps%2C248&sr=8-3",
            //    Amway
            "https://www.amazon.com/STANLEY-FL5W10-Waterproof-Rechargeable-Spotlight/dp/B003UVCY00/ref=sr_1_2?dchild=1&keywords=stanley+black+%26+decker&qid=1595950818&sr=8-2",

            //----          Visit the AmazonBasics Store
            "https://www.amazon.com/AmazonBasics-Compostable-PLA-Hot-500-Count/dp/B07HGW6NZG/ref=sxin_11_pb?cv_ct_cx=cup&dchild=1&keywords=cup&pd_rd_i=B07HGW6NZG&pd_rd_r=b789f048-6e51-4198-9911-355cdd609d62&pd_rd_w=eCyCk&pd_rd_wg=3QVES&pf_rd_p=ffb450f7-bfad-4e7e-95e8-f2bd147e99a4&pf_rd_r=DZJGH8BTJXEZXP4W0EF4&qid=1600777270&sr=1-2-8065ff8c-2587-4a7f-b8da-1df8b2563c11",



//            ----    STORE

            //----          k param
            "https://www.amazon.com/s?k=STANLEY&ref=bl_dp_s_web_0",

            //----          $('meta[property="og:title"]')
            //----          $('[itemprop="itemListElement"]')
            "https://www.amazon.com/stores/AmazonBasics/page/947C6949-CF8E-4BD3-914A-B411DD3E4433?ref_=ast_bln",

            "https://www.amazon.com/stores/Adidas+Originals/page/66B6ABFF-F9DD-48F0-8D81-B45CEB2C6D31?ref_=ast_bln",
            "https://www.amazon.com/stores/page/E869AB6D-D23E-4DAD-80D9-5FA5C580698A?ingress=0&visitId=ef9c9049-36ec-4119-a410-d2ac89cb40ae&ref_=dp_apparel_byline_ety_s&productGridPageIndex=2"

    );

    public static void main(String[] args){
        AmazonBrandNameParser amazonBrandNameReader = new AmazonBrandNameParser();
        TestRunner.run(urls, (siteUrl) -> {
            try {
                rainForestApi(siteUrl);


                var brandInfo = amazonBrandNameReader.getBrandInfo(siteUrl);
                System.out.println(brandInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    private static boolean rainForestApi(String siteUrl) {
        Matcher matcher = Pattern.compile(".*/dp/(\\S+?)/").matcher(siteUrl);
        if (!matcher.find()) {
            return true;
        }
        String asin = matcher.group(1);

        String apiKey = "DEEA363CBE2A475081FF9331F2F93BB4";
        String apiUrl = "https://api.rainforestapi.com/request?api_key=" +
                apiKey +
                "&amazon_domain=amazon.com" +
                "&type=product" +
                "&asin=" + asin +
                "&include_html=false";


        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36");
        headers.set("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");

        var entity = new HttpEntity<>(headers);

        ResponseEntity<String> exchange = restTemplate.exchange(siteUrl, HttpMethod.GET, entity, String.class);
        System.out.println(exchange.getBody());
        return false;
    }
}
