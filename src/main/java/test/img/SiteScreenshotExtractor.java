package test.img;

import lombok.SneakyThrows;

import java.io.IOException;

public interface SiteScreenshotExtractor {
    @SneakyThrows
    SiteProps getSiteImage(String url) throws IOException;
}
