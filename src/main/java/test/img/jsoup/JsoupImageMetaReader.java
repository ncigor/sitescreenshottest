package test.img.jsoup;

import test.img.SiteProps;
import test.img.SiteScreenshotExtractor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * https://ogp.me/
 * https://andrejgajdos.com/how-to-create-a-link-preview/
 */
public class JsoupImageMetaReader implements SiteScreenshotExtractor {
    @Override
    public SiteProps getSiteImage(String siteUrl) throws IOException {
        Document doc = Jsoup.connect(siteUrl)
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36")
                .header("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
                .get();

        String siteImageUrl = getSiteImage(doc);
        String siteTitle = getSiteTitle(doc);
        return SiteProps.builder()
                .siteUrl(siteUrl)
                .imageUrl(siteImageUrl)
                .title(siteTitle)
                .description(getSiteDescription(doc))
                .build();
    }

    private String getSiteImage(Document doc) {
        Optional<String> image = Stream.<Supplier<Optional<String>>>of(
                () -> getContentAttribute(doc, "meta[property='og:image']", "content"),
                () -> getContentAttribute(doc, "link[rel='image_src']", "href"),
                () -> getContentAttribute(doc, "meta[name='twitter:image']", "content")
        ).map(Supplier::get)
                .filter(Optional::isPresent)
                .findFirst()
                .orElse(Optional.empty());
        return image.orElse(null);
    }

    private String getSiteTitle(Document doc) {
        return getContentAttribute(doc, "meta[property='og:title']", "content")
                .orElseGet(doc::title);     // select("head title").text()
    }

    private String getSiteDescription(Document doc) {
        return getContentAttribute(doc, "meta[property='og:description']", "content")
                .orElseGet(() -> getMetaDescription(doc));     // select("head title").text()
    }

    private String getMetaDescription(Document doc) {
        return getContentAttribute(doc, "meta[name='description']", "content").orElse(null);
    }

    private Optional<String> getContentAttribute(Document doc, String cssQuery, String attributeKey) {
        return Optional.of(doc.select(cssQuery))
                .filter(elements -> elements.size() > 0)
                .map(elements -> elements.get(0))
                .map(element -> element.attr(attributeKey));
    }

}
