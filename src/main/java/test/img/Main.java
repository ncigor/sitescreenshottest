package test.img;

import test.TestRunner;
import test.img.jsoup.JsoupImageMetaReader;
import test.img.selenium.WebsiteCrawler;

import java.io.IOException;
import java.util.List;

public class Main {
    static List<String> urls = List.of(
            "https://www.amazon.com/dp/B07TMJ1R3X/ref=ods_gw_d_h1_tab_ox_sept_tpr",
            "https://stackoverflow.com/questions/1504034/take-a-screenshot-of-a-web-page-in-java",
            "https://www.ukr.net/",
            "https://www.amazon.com/GoPro-HERO8-Digital-Action-Camera/dp/B081GG63NR/ref=pd_sbs_3",
            "https://en.wikipedia.org/wiki/Site",
            "https://www.google.com/gmail/",
            "https://www.nike.com/",
            "https://www.nike.com/eg/",
            "https://www.conductor.com/",
            "https://web.dev/measure/?gclid=Cj0KCQjwnqH7BRDdARIsACTSAdvG0hR-WnhVsGERDKSas1-7EKfsjhb5R5eIJZWKOOWs8PHVQLK7I8waAvjQEALw_wcB",
            "https://moz.com/beginners-guide-to-seo",
            "https://en.wikipedia.org/wiki/Search_engine_optimization",
            "https://searchengineland.com/guide/what-is-seo",
            "https://neilpatel.com/what-is-seo/",
            "https://www.wordstream.com/blog/ws/2015/04/30/seo-basics",
            "https://www.searchenginewatch.com/2016/01/21/seo-basics-22-essentials-you-need-for-optimizing-your-site/",
            "https://www.google.com/search?q=seo&oq=seo&aqs=chrome..69i57j35i39j0l2j46j0j69i61j69i65.9126j0j7&sourceid=chrome&ie=UTF-8",
            "https://medium.com/@NeotericEU/single-page-application-vs-multiple-page-application-2591588efe58",
            "https://dzone.com/articles/what-is-a-single-page-application",
            "https://developer.mozilla.org/en-US/docs/Glossary/SPA",
            "https://testropfresh.fr/"
    );


    public static void main(String[] args){
        final SiteScreenshotExtractor imageReader = getSiteScreenshotExtractor();

        TestRunner.run(urls, (siteUrl) -> {
            try {
                SiteProps props = imageReader.getSiteImage(siteUrl);
                System.out.println(props);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    private static SiteScreenshotExtractor getSiteScreenshotExtractor() {
        SiteScreenshotExtractor imageReader;
        imageReader = new JsoupImageMetaReader();
        imageReader = new WebsiteCrawler();
        return imageReader;
    }


}
