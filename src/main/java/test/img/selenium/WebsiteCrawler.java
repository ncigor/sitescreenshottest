package test.img.selenium;

import com.google.common.io.Files;
import test.img.SiteProps;
import test.img.SiteScreenshotExtractor;
import lombok.SneakyThrows;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

/**
 * https://www.carrotstack.com/blog/building-website-capture-tool
 */
public class WebsiteCrawler implements SiteScreenshotExtractor {

    private WebDriver driver;

    public WebsiteCrawler() {
        // Define the location of the chromedriver
        System.setProperty("webdriver.chrome.driver", getResourcePath());
        // Use headless mode for the ChromeDriver
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        this.driver = new ChromeDriver(chromeOptions);
    }

    @SneakyThrows
    @Override
    public SiteProps getSiteImage(String url) {
        // Navigate to the specified directory
        driver.navigate().to(url);
        String title = driver.getTitle();
        // Sleep for 5 seconds in case the website has not fully loaded
//        Thread.sleep(2000);
        // Take the screenshot and copy to the specified directory
        File screenshotAs = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File file = new File(url.hashCode() + ".png");
        Files.copy(screenshotAs, file);
        return SiteProps.builder()
                .title(title)
                .siteUrl(url)
                .imageUrl(file.getAbsolutePath())
                .build();
    }

    public void close() {
        // Close after completion
        driver.close();
    }

    private String getResourcePath() {
        return getClass().getResource("/chromedriver/chromedriver").getPath();
    }

}