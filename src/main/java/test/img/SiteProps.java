package test.img;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
//https://andrejgajdos.com/how-to-create-a-link-preview/
public class SiteProps {
    String siteUrl;
    String title;
    String description;
    String imageUrl;
}
