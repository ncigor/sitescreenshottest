package test.img.awt;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class ScreeScreenshot {

    public static void main(String[] args) throws AWTException, IOException {
        Robot robot = new Robot();
        // Capture the screen shot of the area of the screen defined by the rectangle
        BufferedImage bi = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        File output = new File(new Random().nextInt() + ".jpg");
        System.out.println(output.getAbsolutePath());
        ImageIO.write(bi, "jpg", output);
    }
}