package test;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class TestRunner {
    public static void run(List<String> urls, Consumer<String> consumer) {
        StopWatch stopWatch = new StopWatch();
        for (String siteUrl : urls) {
            var start = getTimeSecond();
            stopWatch.start(siteUrl);
            consumer.accept(siteUrl);
            var time = getTimeSecond() - start;
            System.out.println(format(time) + "sec: \t");
            System.out.println("=====");
            stopWatch.stop();
        }
        System.out.println(stopWatch.prettyPrint());
    }

    private static String format(double time) {
        return NumberFormat.getInstance().format(time);
    }

    private static double getTimeSecond() {
        return System.currentTimeMillis() / 1000d;
    }

    public static class StopWatch extends org.springframework.util.StopWatch {
        private static final String STRING_DELIMITER = "---------------------------------------------\n";
        private static final int TASKNAME_LENGTH = 50;
        private final NumberFormat nf = NumberFormat.getNumberInstance();

        public String prettyPrint() {
            StringBuilder sb = new StringBuilder();
            sb.append(getSummaryStats());
            sb.append(STRING_DELIMITER);
            sb.append("sec        %     Task name\n");
            sb.append(STRING_DELIMITER);
            for (TaskInfo task : getTaskInfo()) {
                sb.append(nf.format(toSec(task.getTimeNanos()))).append(" \t ");
                sb.append(nf.format((double) task.getTimeNanos() / getTotalTimeNanos())).append(" \t ");
                String taskName = trim(task.getTaskName());
                sb.append(taskName).append("\n");
            }
            return sb.toString();
        }

        @Override
        public void start(String taskName) throws IllegalStateException {
            super.start(taskName);
        }

        private static String trim(String siteUrl) {
            String trimmed = siteUrl.substring(0, Integer.min(siteUrl.length(), TASKNAME_LENGTH));
            if (!trimmed.equals(siteUrl)) {
                trimmed += "...";
            }
            return trimmed;
        }

        private String getSummaryStats() {
            var stats = Arrays.stream(getTaskInfo())
                    .mapToDouble(TaskInfo::getTimeSeconds)
                    .summaryStatistics();
            return String.format(STRING_DELIMITER +
                            "count:\t%d\n" +
                            "total:\t%f sec\n" +
                            "sum:\t%f sec\n" +
                            "min:\t%f sec\n" +
                            "average:\t%f sec\n" +
                            "max:\t%f sec\n ",
                    stats.getCount(),
                    toSec(getTotalTimeNanos()),
                    stats.getSum(),
                    stats.getMin(),
                    stats.getAverage(),
                    stats.getMax());
        }

        private double toSec(long timeNanos) {
            return timeNanos / 1_000_000_000.0;
        }
    }
}
